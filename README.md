# atsExplosivePotion

The plugin allows to craft the splash potion, that explodes once it hits anything with set power of explosion.

## Details

You can define any amount of potions in the `config.yml` file. You are able to configure the potion item as
an `ItemStack`, which means you can set its name and lore. Additionally, you can set the explosion power and own
crafting recipe (they **have to** be unique)!

An example config that includes a potion is present as a default config.

## Downloading

You can download the plugin
from [spigotmc.org/atsExplosivePotion](https://www.spigotmc.org/resources/atsexplosivepotion-craft-throwable-potion-that-explodes.96224/ "atsExplosivePotion")

## Contributing

If you wish to contribute it with me, just send the join request :)
